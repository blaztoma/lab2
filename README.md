# README #

Antro laboratorinio darbo trumpas aprašymas.

### Laboratorinio darbo temos ###

* vartotojo klasės metodu ToString(), paveldėtu iš klasės Object ir užklotu vartotojo klasėje;
* eilučių formavimo operatoriumi string.Format();
* klase įtraukiančia kitų klasių objektus;
* palyginimo ir loginių operacijų užklojimu;
* rikiavimo, pašalinimo algoritmais;

### Kaip pasileisti ###

* Atsisiųskite projektą iš saugyklos
* Išarchyvuokite projektų kataloge
* Atidarykite su Microsoft Visual Studio programavimo aplinka

### Kaip patobulinti ###

Radus klaidą:
* Pasakykite kuruojančiam dėstytojui arba
* rašykite adresu blaztoma@gmail.com